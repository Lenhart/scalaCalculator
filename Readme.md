# Project Title
Scala Calculator

## Installation
You'll need to have scala and sbt installed.
Install scala:
```
brew install scala
```
Install sbt:
```
brew install sbt
```

## Run tests
```
cd PROJECT
sbt
> test
```

## Run
```
cd PROJECT
sbt
> run
```


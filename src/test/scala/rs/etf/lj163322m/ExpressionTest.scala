package test.scala.rs.etf.lj163322m

import org.scalatest._
import org.scalactic.TolerantNumerics
import main.scala.rs.etf.lj163322m.expression._

class ExpressionTest extends FlatSpec with Matchers {
  implicit val doubleEq = TolerantNumerics.tolerantDoubleEquality(1e-4f)
  
  "Constant" should "return value it was created with" in {
    val first = new Constant(1); // integer
    assert(first(0) === 1.0)
    assert(first(0) === 1.0) // Intentionally called twice for the same object
    assert(new Constant(-4.4)() === -4.4) // real
  }
  
  "Add" should "return value of first + second expression" in {
    val i = new Constant(1);
    val r = new Constant(-2.2);
    assert(new Add(i, i)() === 2.0);
    assert(new Add(r, r)() === -4.4);
    assert(new Add(r, r)() === -4.4);
  }
  
  "Sub" should "return value of first - second expression" in {
    val small = new Constant(1);
    val big = new Constant(10);
    assert(new Sub(big, small)() === 9.0);
    assert(new Sub(small, big)() === -9.0);
  }
  
  "Mul" should "return first * second expression" in {
    val pos = new Constant(2);
    val neg = new Constant(-3);
    val zero = new Constant(0);
    
    assert(new Mul(pos, pos)() === 4.0);
    assert(new Mul(pos, neg)() === -6.0);
    assert(new Mul(neg, neg)() === 9.0);
    assert(new Mul(pos, zero)() === 0.0);
  }
  
  "Div" should "return first / second" in {
    val a = new Constant(2);
    val b = new Constant(1);
    val c = new Constant(-3);
    val z = new Constant(0);
    assert(new Div(a, b)() === 2.0);
    assert(new Div(a, c)() === -0.66666);
    assert(new Div(z, c)() === 0.0);
  }
  it should "give you infinity if divided by zero" in {
    val a = new Constant(1);
    val z = new Constant(0);
    assert(new Div(a, z)() === Double.PositiveInfinity);
  }
  
  "Pow" should "return first raised to power of second expressions" in { // association right to left is not handled
    val l = new Constant(1);
    val t = new Constant(2);
    assert(new Pow(l, l)() === 1.0);
    assert(new Pow(l, t)() === 1.0);
    assert(new Pow(t, l)() === 2.0);
    assert(new Pow(t, t)() === 4.0);
  }
  
  "FunEx" should "return value of associated function" in {
    val z = new Constant(0)
    assert(new FunEx("sin", z)() === 0.0);
    assert(new FunEx("cos", z)() === 1.0);
    assert(new FunEx("", z)() === 0.0);
  }
  it should "throw exception when no function associated" in {
    val z = new Constant(0)
    assertThrows[Exception](new FunEx("undefined", z)());
  }
  
  "NamedLocalVar" should "return value it was set up with" in {
    val c1 = Constant(1)
    val namedVar = new NamedLocalVar("$var11", c1); // value set
    assert(namedVar(0) === 1.0);
    val namedVar2 = new NamedLocalVar("$var11"); // value read
    assert(namedVar2(0) === 1.0);
  }
  it can "change value" in {
    val c2 = Constant(2)
    val c3 = Constant(3)
    val namedVar = new NamedLocalVar("$var12", c2)
    assert(namedVar(0) === 2.0)
    val namedVar2 = new NamedLocalVar("$var12", c3)
    assert(namedVar(0) === 3.0)
    assert(namedVar2(0) === 3.0)
  }
  
  "NamedVal" should "return value it was set up with" in {
    val c1 = Constant(1)
    val namedVal = new NamedVal("VAL11", c1); // value set
    assert(namedVal(0) === 1.0);
    val namedVal2 = new NamedVal("VAL11"); // value read
    assert(namedVal2(0) === 1.0);
  }
  it must "throw exception when trying to change value" in {
    val c2 = Constant(2)
    val c3 = Constant(3)
    val namedVal = new NamedVal("VAL12", c2)
    assert(namedVal(0) === 2.0)
    assertThrows[Exception](new NamedVal("VAL12", c3))
    assertThrows[Exception](new NamedVal("VAL12", c2)) // even when setting same value
  }
  
  "NamedVar" should "return passed value" in {
    val namedVarDef = new NamedVar("x11", true)
    val namedVar = new NamedVar("x11")
    assert(namedVar(0) === 0.0)
    assert(namedVar(42) === 42.0)
    assert(namedVar(-1) === -1.0)
  }
  it should "throw exception if using without definition" in {
    val namedVar = new NamedVar("x12")
    assert(namedVar(0) === 0.0)
    assert(namedVar(42) === 42.0)
    assert(namedVar(-1) === -1.0)
  }
  
  "FunDef" should "register a new function" in {
    val z = new Constant(0)
    val c1 = new Constant(1)
    val namedVar = new NamedVar("x13", true)
    
    new FunDef("myFunc0$", (x) => 3)
    assert(new FunEx("myFunc0$", z)() === 3.0);
    assert(new FunEx("myFunc0$", c1)() === 3.0);
    
    new FunDef("myFunc1$", (x) => x)
    assert(new FunEx("myFunc1$", z)() === 0.0);
    assert(new FunEx("myFunc1$", c1)() === 1.0);
  }
  
  "Integ" should "return integrated value of given function name over given range" in {
    val c1 = Constant(1)
    val c5 = Constant(5)
    val integ = new Integ("signum", c1, c5)
    assert(Math.abs(integ() - 4.0) < 0.1)
    val integBack = new Integ("signum", c5, c1)
    assert(Math.abs(integBack() - 4.0) < 0.1)
  }
  
  "EquDef" should "register new equation and return value of its free element" in {
    UnEqu()
    val c1: Expression = Constant(1)
    val c2: Expression = Constant(2)
    
    val tuple1 = (c1, "x")
    val array1 = Array(tuple1)
    assert(new EquDef(array1, c2)() === 2.0)
    
    val tuple2 = (c2, "y")
    val array2 = Array(tuple2)
    assert(new EquDef(array2, c1)() === 1.0)
  }
  
  "Solve" should "solve system of specified equations" in {
    // Solve system of 1 equation with 1 unknown
    UnEqu()
    val c1: Expression = Constant(1)
    val c42 = Constant(42)
    val tuple1 = (c1, "x")
    val array1 = Array(tuple1)
    new EquDef(array1, c42) // x = 42
    
    val first = Array(1)
    assert(new Solve(first)() === 42.0)
    
    // Solve system of 2 equations with 2 unknowns
    val c2 = Constant(2)
    val array2 = Array((c1, "x"), (c1, "y"))
    new EquDef(array2, c1) // x + y = 1
    
    val array3 = Array((c2, "x"), (c1, "y"))
    new EquDef(array3, c2) // 2x + y = 2

    val secondAnd3rd = Array(2, 3)
    assert(new Solve(secondAnd3rd)() === 1.0) // return solution for the first unknown
  }
  it should "throw exception when system is too loose" in {
    UnEqu()
    val c1: Expression = Constant(1)
    val c2 = Constant(2)
    val array3 = Array((c2, "x"), (c1, "y"))
    new EquDef(array3, c2) // 2x + y = 2
    new EquDef(array3, c2) // the same
    val firstAnd2nd = Array(1, 2)
    val thrown = intercept[Exception] {
      new Solve(firstAnd2nd)()
    }
    assert(thrown.getMessage === "System is too loose, there's infinite number of solutions")
  }
  it should "throw exception when system is too strict" in {
    UnEqu()
    val c1: Expression = Constant(1)
    val c2 = Constant(2)
    val array = Array((c1, "x"), (c1, "y"))
    new EquDef(array, c1) // x + y = 1
    new EquDef(array, c2) // x + y = 2
    val firstAnd2nd = Array(1, 2)
    val thrown = intercept[Exception] {
      new Solve(firstAnd2nd)()
    }
    assert(thrown.getMessage === "System is too strict, there's no solution")
  }
}
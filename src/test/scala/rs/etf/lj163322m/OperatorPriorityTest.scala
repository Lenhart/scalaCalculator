package test.scala.rs.etf.lj163322m

import org.scalatest._
import org.scalactic.TolerantNumerics
import main.scala.rs.etf.lj163322m.expression._
import main.scala.rs.etf.lj163322m.parser._

class OperatorPriorityTest extends FlatSpec with Matchers {
  
  implicit val doubleEq = TolerantNumerics.tolerantDoubleEquality(1e-4f)
  
  "Application" can "parse and calculate value of various simple expressions" in {
    assert(Parser.parse("27")() === 27.0);
    assert(Parser.parse("-2.3")() === -2.3);
    assert(Parser.parse("2 + 3")() === 5.0);
    assert(Parser.parse("7 - 9")() === -2.0);
    assert(Parser.parse("3 * 9")() === 27.0);
    assert(Parser.parse("9 / 4")() === 2.25);
    assert(Parser.parse("2 ^ 7")() === 128.0);
  }
  it can "parse and calculate a more complex expressions with same priority" in {
    assert(Parser.parse("1 + 2 + 3")() === 6.0);
    assert(Parser.parse("2 + 4 - 8")() === -2.0);
    assert(Parser.parse("2 * 3 * 4")() === 24.0);
    assert(Parser.parse("2 * 3 / 4")() === 1.5);
    assert(Parser.parse("4 / 2 * 3")() === 6.0);
    assert(Parser.parse("2 ^ 3 ^ 2")() === 64.0); // Intentionally, didn't handle association
  }
  it can "parse and calculate more complex expressions with operators of different priority" in {
    assert(Parser.parse("2 + 3 * 4")() === 14.0);
    assert(Parser.parse("3 * 4 + 2")() === 14.0);
    assert(Parser.parse("2 * 3 ^ 3")() === 54.0);
    assert(Parser.parse("3 ^ 3 * 2")() === 54.0);
    assert(Parser.parse("2 + 2 * 3 ^ 3 - 20")() === 36.0);
  }
  it can "parse and calculate expressions with functions" in {
    assert(Parser.parse("sin(0) + 12")() === 12.0);
    assert(Parser.parse("3 + cos(0) * 12")() === 15.0);
    assert(Parser.parse("3 + cos(5 - 5) * 12")() === 15.0);
    assert(Parser.parse("3 + cos(0) + sin(0)")() === 4.0);
    assert(Parser.parse("3 + cos(sin(7 - 7)) * cos(3 * 0)")() === 4.0);
  }
  it can "parse and calculate expressions with priorities set by parentheses" in {
    assert(Parser.parse("(3)")() === 3.0);
    assert(Parser.parse("(14 - 29.7)")() === -15.7);
    assert(Parser.parse("2 * (sin(0) + 2)")() === 4.0);
    assert(Parser.parse("2 * (sin(3 * (1 - 1)) + 2)")() === 4.0);
  }
  it can "parse and calculate expressions using local variables and constants" in {
    Parser.parse("$var30 = 20")
    Parser.parse("VAL30 = 22")
    assert(Parser.parse("$var30 + VAL30")() === 42.0);
    assert(Parser.parse("2 * ($var30 + VAL30)")() === 84.0);
  }
  it can "parse and calculate expressions using variables" in {
    Parser.parse("def x20")
    assert(Parser.parse("x20")(0) === 0.0)
    assert(Parser.parse("x20")() === 0.0)
    assert(Parser.parse("x20")(42) === 42.0)
    assert(Parser.parse("x20")(-1) === -1.0)
  }
  it can "use user-defined functions" in {
    Parser.parse("def x21")
    Parser.parse("f21$ = x21")
    assert(Parser.parse("f21(0)")() === 0.0)
    assert(Parser.parse("f21(1)")() === 1.0)
    
    Parser.parse("f22$ = 2 * (1 + x21)")
    assert(Parser.parse("f22(0)")() === 2.0)
    assert(Parser.parse("f22(3)")() === 8.0)
  }
  it can "parse and calculate integral" in {
    val integValue = Parser.parse("integ signum 1 4")()
    val expected = 3.0
    assert(Math.abs(integValue - expected) < 0.1)
  }
  it can "parse and solve system of equations" in {
    Parser.parse("uneq")()
    Parser.parse("eq x33 = 3")
    assert(Parser.parse("solve 1")() === 3.0)
    
    Parser.parse("eq x33 + y33 = 1")
    Parser.parse("eq 2 * x33 + y33 = 2")
    assert(Parser.parse("solve 2 3")() === 1.0)
  }
}
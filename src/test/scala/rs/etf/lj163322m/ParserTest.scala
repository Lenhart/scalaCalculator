package test.scala.rs.etf.lj163322m

import org.scalatest._
import org.scalactic.TolerantNumerics
import main.scala.rs.etf.lj163322m.parser._
import main.scala.rs.etf.lj163322m.expression._

class ParserTest extends FlatSpec with Matchers {
  
  implicit val doubleEq = TolerantNumerics.tolerantDoubleEquality(1e-4f)
  
  "Parser" should "return null for empty string" in {
    val op = Parser.parse("");
    assert(op === NaN);
    
    val op2 = Parser.parse(" ");
    assert(op2 === NaN);
  }
  it can "parse constant real values" in {
    assert(Parser.parse("27").isInstanceOf[Constant]);
    assert(Parser.parse("27.").isInstanceOf[Constant]);
    assert(Parser.parse("1.2").isInstanceOf[Constant]);
    assert(Parser.parse("-2.5").isInstanceOf[Constant]);
    assert(Parser.parse("1e3").isInstanceOf[Constant]);
    assert(Parser.parse("0.000").isInstanceOf[Constant]);
  }
  it can "parse constant integers" in {
    assert(Parser.parse("0").isInstanceOf[Constant]);
    assert(Parser.parse("1").isInstanceOf[Constant]);
    assert(Parser.parse("65535").isInstanceOf[Constant]);
    assert(Parser.parse("-1").isInstanceOf[Constant]);
  }
  it can "parse sum and sub expression" in {
    assert(Parser.parse("1 + 1").isInstanceOf[Add])
    assert(Parser.parse("3.0 + 3.3").isInstanceOf[Add])
    assert(Parser.parse("1 - 1").isInstanceOf[Sub])
    assert(Parser.parse("3.0 - 3.3").isInstanceOf[Sub])
  }
  it can "parse mul and div expression" in {
    assert(Parser.parse("1 * 2").isInstanceOf[Mul])
    assert(Parser.parse("1.1 * 3.3").isInstanceOf[Mul])
    assert(Parser.parse("3 / 2").isInstanceOf[Div])
    assert(Parser.parse("3.4 / 3.4").isInstanceOf[Div])
  }
  it can "parse pow expression" in {
    assert(Parser.parse("1 ^ 2").isInstanceOf[Pow])
    assert(Parser.parse("3 ^ 2").isInstanceOf[Pow])
    assert(Parser.parse("3 ^ 3").isInstanceOf[Pow])
    assert(Parser.parse("2.0 ^ 2.0").isInstanceOf[Pow])
    assert(Parser.parse("0.2 ^ 2").isInstanceOf[Pow])
  }
  it can "parse various functions" in {
    assert(Parser.parse("sin(1)").isInstanceOf[FunEx])
    assert(Parser.parse("cos(1)").isInstanceOf[FunEx])
    assert(Parser.parse("tan(1 + 2)").isInstanceOf[FunEx])
    assert(Parser.parse("undefined(1)").isInstanceOf[FunEx])
  }
  it can "parse priorities set by parentheses" in {
    assert(Parser.parse("(1)").isInstanceOf[FunEx])
    assert(Parser.parse("(1 + 1)").isInstanceOf[FunEx])
    assert(Parser.parse("2 * (sin(0) + 2)").isInstanceOf[Mul])
  }
  it can "parse definition of variable or named constant" in {
    assert(Parser.parse("$var01=1").isInstanceOf[NamedLocalVar])
    assert(Parser.parse("$var02 = 3 * 2").isInstanceOf[NamedLocalVar])
    assert(Parser.parse("VAL01 = 2.0").isInstanceOf[NamedVal])
    assert(Parser.parse("VAL02 = sin(20)").isInstanceOf[NamedVal])
  }
  it can "parse definition and usage of unknown variable" in {
    assert(Parser.parse("def x01").isInstanceOf[NamedVar])
    assert(Parser.parse("2 * x01").isInstanceOf[Mul])
  }
  it can "parse expressions using variables and named constants" in {
    assert(Parser.parse("$var03").isInstanceOf[NamedLocalVar])
    assert(Parser.parse("3 + $var04").isInstanceOf[Add])
    assert(Parser.parse("sin(3 + $var05)").isInstanceOf[FunEx])
    assert(Parser.parse("VAL03").isInstanceOf[NamedVal])
    assert(Parser.parse("3 + VAL04").isInstanceOf[Add])
    assert(Parser.parse("sin(3 + VAL05)").isInstanceOf[FunEx])
  }
  it can "parse definition of function" in {
    Parser.parse("def x02")
    assert(Parser.parse("myFunc0$ = 3").isInstanceOf[FunDef])
    assert(Parser.parse("myFunc1$ = x02").isInstanceOf[FunDef])
    assert(Parser.parse("myFunc1$ = 2 * (1 + x02)").isInstanceOf[FunDef])
    assert(Parser.parse("myFunc1$ = sin(x02)").isInstanceOf[FunDef])
  }
  it can "parse expressions for integrating function over range" in {
    assert(Parser.parse("integ sin 1 2").isInstanceOf[Integ])
    assert(Parser.parse("integ sin $var06 VAL06").isInstanceOf[Integ])
  }
  it can "parse linear equations" in {
    Parser.parse("def x03")
    assert(Parser.parse("eq x03 = 0").isInstanceOf[EquDef])
    assert(Parser.parse("eq x03 = -1").isInstanceOf[EquDef])
    assert(Parser.parse("eq 2 * x03 = 3").isInstanceOf[EquDef])
    assert(Parser.parse("eq -2 * x03 = 4").isInstanceOf[EquDef])
    Parser.parse("def y03")
    assert(Parser.parse("eq x03 + y03 = 5").isInstanceOf[EquDef])
    assert(Parser.parse("eq x03 + -1 * y03 = 5").isInstanceOf[EquDef])
    assert(Parser.parse("eq -1 * x03 + -2 * y03 = 5").isInstanceOf[EquDef])
    Parser.parse("def z03")
    assert(Parser.parse("eq x03 + y03 + z03 = 6").isInstanceOf[EquDef])
    assert(Parser.parse("eq -1 * x03 + -1 * y03 + -1 * z03 = 6").isInstanceOf[EquDef])
    assert(Parser.parse("eq -1 * sin(1) * x03 + exp(2) * y03 + (1 - 2 + log(2)) * z03 = 6").isInstanceOf[EquDef])
  }
  it can "parse command to wipe out all equations defined" in {
    assert(Parser.parse("uneq") === UnEqu)
  }
  it can "parse command to solve system of linear equations" in {
    assert(Parser.parse("solve 1").isInstanceOf[Solve])
    assert(Parser.parse("solve 2 3").isInstanceOf[Solve])
    assert(Parser.parse("solve 5 6 7").isInstanceOf[Solve])
  }
}
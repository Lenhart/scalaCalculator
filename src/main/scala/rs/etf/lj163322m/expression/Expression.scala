package main.scala.rs.etf.lj163322m.expression

import scala.math
import scala.collection.mutable.HashMap
import main.scala.rs.etf.lj163322m.calculator._

abstract sealed class Expression extends Function1[Double, Double] {
  def apply(): Double = apply(0)
}

final case object NaN extends Expression {
  def apply(x: Double) = Double.NaN
}

final case class Constant(value: Double) extends Expression {
  def apply(x: Double) = value
}

final case class Add(e1: Double => Double, e2: Double => Double) extends Expression {
  def apply(x: Double) = e1(x) + e2(x);
}

final case class Sub(e1: Double => Double, e2: Double => Double) extends Expression {
  def apply(x: Double) = e1(x) - e2(x);
}

final case class Mul(e1: Double => Double, e2: Double => Double) extends Expression {
  def apply(x: Double) = e1(x) * e2(x);
}

final case class Div(e1: Double => Double, e2: Double => Double) extends Expression {
  def apply(x: Double) = e1(x) / e2(x);
}

final case class Pow(e1: Double => Double, e2: Double => Double) extends Expression {
  def apply(x: Double) = scala.math.pow(e1(x), e2(x));
}

final case class FunEx(name: String, param: Double => Double) extends Expression {
  def apply(x: Double) = {
    val func = Calculator.funcByName.get(name);
    func match {
      case Some(f) => {
        val paramValue = param(x);
        f(paramValue);
      }
      case None => throw new Exception("Undefined function: " + name)
    }
  }
}

sealed class NamedValue(name: String, fixed: Boolean, function: Double => Double) extends Expression {
  if (function != null) {
    if (fixed) {
      if (Calculator.valueByName.contains(name)) throw new Exception("Named " + this + " can't change value!")
    }
    val entry = (fixed, function)
    Calculator.valueByName.put(name, entry)
  }
  def apply(x: Double) = {
    val entry = Calculator.valueByName.get(name)
    entry match {
      case Some(record) => record._2(x)
      case None => throw new Exception(s"$this named '$name' couldn't be found!")
    }
  }
}

final case class NamedLocalVar(name: String, function: Double => Double) extends NamedValue(name, false, function) {
  def this(name: String) = this(name, null)
  override def toString() = "Local variable"
}

final case class NamedVal(name: String, function: Double => Double) extends NamedValue(name, true, function) {
  def this(name: String) = this(name, null)
  override def toString() = "Constant"
}

final case class NamedVar(name: String, define: Boolean) extends NamedValue(name, false, (x) => x) {
  def this(name: String) = this(name, false)
  override def toString() = "Variable";
}

final case class FunDef(name: String, function: Double => Double) extends Expression {
  val previous = Calculator.funcByName.put(name, function);
  previous match {
    case Some(f) => println(s"Note: You've just overwrote previous definition of function named $name");
    case None =>
  }

  def apply(x: Double) = 0 // Important is only side-effect in constructor
}

final case class Integ(funcName: String, from: Double => Double, to: Double => Double) extends Expression {
  def apply(x: Double) = {
    Calculator.integrate(funcName, from(0), to(0));
  }
}

final case class EquDef(parts: Array[(Expression, String)], value: Expression) extends Expression {
  Calculator.equations += this
  def apply(x: Double) = value(x)
  
  override def toString() = {
    var line = ""
    for (part <- parts) {
      if (line != "") {
        line += " + "
      }
      var coefValue = part._1()
      if (coefValue != 1) {
        line += s"$coefValue * " + part._2
      } else {
        line += part._2
      }
    }
    line + " = " + value()
  }
}

final case object UnEqu extends Expression {
  def apply(x: Double) = {
    Calculator.equations.clear()
    0
  }
}

final case class Solve(indices: Array[Int]) extends Expression {
  def apply(x: Double) = {
    val solution = Calculator.solve(indices)
    
    println("Solution: ")
    for (solvedVar <- solution) {
      println(solvedVar._1 + " = " + solvedVar._2)
    }
    println("")
    
    solution(0)._2
  }
}

package main.scala.rs.etf.lj163322m.parser

import main.scala.rs.etf.lj163322m.command._
import main.scala.rs.etf.lj163322m.expression._
import java.util.regex.Pattern
import scala.collection.mutable.HashMap

object Parser {
  
  var parenthCount = 0
  var substitutions = new HashMap[String, Expression]();
  
  def parse(input: String): Expression = {
    val trimmed = input.trim
    
    val integPattern = """^integ ([a-zA-Z][a-zA-Z\d]*) (.+) (.+)$""".r
    val funDefPattern = """^([a-zA-Z][a-zA-Z\d]*)\$ = (.*)$""".r
    val equDefPattern = """^eq (.+) = (.+)$""".r
    val solvePattern = """^solve ( ?\d+)+ ?$""".r
    val localVarDefPattern = """^(\$[a-z][a-z\d]*) *= *(.+)$""".r
    val valDefPattern = """^([A-Z][A-Z\d]*) *= *(.+)$""".r
    val varDefPattern = """^def ([a-z][a-z\d]*)$""".r
    val parenthPattern = """^(.*)\(([^\(\<>)]+)\)(.*)$""".r
    val parenth2Pattern = """^(.*)\((.*\(<\d+>\)[^\)]*)\)(.*)$""".r
    
    trimmed match {
      case integPattern(funcName, fromS, toS) => {
        val from = parse(fromS)
        val to = parse(toS)
        new Integ(funcName, from, to)
      }
      case funDefPattern(name, definition) => {
        val function = parse(definition);
        new FunDef(name, function)
      }
      case equDefPattern(equation, value) => {
        parseEquation("+ " + equation + " +", value)
      }
      case solvePattern(oneGroup) => {
        val simpleLine = trimmed.replaceAll(" +", " ")
        val simplePattern = """\d+""".r
        val matches = simplePattern.findAllIn(simpleLine)

        var indices = Array.emptyIntArray
        for (v <- matches) {
          indices = indices :+ v.toInt
        }
        
        new Solve(indices)
      }
      case localVarDefPattern(name, exp) => {
        val expression = parse(exp)
        new NamedLocalVar(name, expression)
      }
      case valDefPattern(name, exp) => {
        val expression = parse(exp)
        new NamedVal(name, expression)
      }
      case varDefPattern(name) => {
        new NamedVar(name, true)
      }
      case parenthPattern(pre, in, post) => {
        val inEx = parseSimpleExpression(in);
        val key = "<" + parenthCount + ">"
        substitutions.put(key, inEx);
        parenthCount += 1
        val newInput = pre + "(" + key + ")" + post
        parse(newInput)
      }
      case parenth2Pattern(pre, in, post) => {
        val inEx = parseSimpleExpression(in);
        val key = "<" + parenthCount + ">"
        substitutions.put(key, inEx);
        parenthCount += 1
        val newInput = pre + "(" + key + ")" + post
        parse(newInput)
      }
      case _ => parseSimpleExpression(trimmed);
    }
  }
  
  def parseSimpleExpression(input: String): Expression = {
    val trimmed = input.trim
    
    val intPattern = """^([+-]?\d+)$""".r
    val doublePattern = """^([+-]?\d+[.e]\d*)$""".r
    val localVarUsePattern = """^(\$[a-z][a-z\d]*)$""".r
    val valUsePattern = """^([A-Z][A-Z\d]*)$""".r
    val varUsePattern = """^([a-z][a-z\d]*)$""".r
    val addPattern = """^(.+) \+ (.+)$""".r
    val subPattern = """^(.+) \- (.+)$""".r
    val mulPattern = """^(.+) \* (.+)$""".r
    val divPattern = """^(.+) / (.+)$""".r
    val powPattern = """^(.+) \^ (.+)$""".r
    val funcPattern = """^([a-zA-Z\d]*)\((<\d+>)\)$""".r
    
    trimmed match {
      case "" => NaN
      case intPattern(s) => {
        val x = s.toInt
        new Constant(x);
      }
      case doublePattern(s) => {
        val x = s.toDouble
        new Constant(x);
      }
      case "uneq" => {
        UnEqu
      }
      case localVarUsePattern(s) => {
        new NamedLocalVar(s)
      }
      case valUsePattern(s) => {
        new NamedVal(s)
      }
      case varUsePattern(s) => {
        new NamedVar(s)
      }
      case addPattern(l, r) => {
        val left = Parser.parse(l);
        val right = Parser.parse(r);
        new Add(left, right);
      }
      case subPattern(l, r) => {
        val left = Parser.parse(l);
        val right = Parser.parse(r);
        new Sub(left, right);
      }
      case mulPattern(l, r) => {
        val left = Parser.parse(l);
        val right = Parser.parse(r);
        new Mul(left, right);
      }
      case divPattern(l, r) => {
        val left = Parser.parse(l);
        val right = Parser.parse(r);
        new Div(left, right);
      }
      case powPattern(l, r) => {
        val left = Parser.parse(l);
        val right = Parser.parse(r);
        new Pow(left, right);
      }
      case funcPattern(funName, paramKey) => {
        substitutions.get(paramKey) match {
          case Some(param) => new FunEx(funName, param)
          case None => throw new Exception("Substitution couldn't be found for key: " + paramKey + " in line: " + trimmed)
        }
      }
      case _ => {
        println("Failed parsing: '" + trimmed + "'")
        NaN
        }
    }
  }
  
  def parseEquation(startingEqu: String, valueS: String): Expression = {
    // Parse coefficients and names of vars
    var coefsAndNames: Array[(Expression, String)] = Array()
    
    val coefPattern = """^.*[a-zA-Z][a-zA-Z\d]* \+$"""
    var equLeft = startingEqu
    var cnt = 0
    while (equLeft != "") {
      cnt += 1
      val considerCoef = equLeft.substring(0, cnt)
      
      if (considerCoef.matches(coefPattern)) {
        val coefAndVarPattern = """^\+? (.* \* )?([a-zA-Z][a-zA-Z\d]*) \+?$""".r
          considerCoef match {
            case coefAndVarPattern(coefS, name) => {
              val coef = if (coefS != null) {
                coefS match {
                  case "" => Constant(1)
                  case s: String if s.endsWith(" * ") => parse(coefS.dropRight(3))
                  case _ => parse(coefS)
                }
              } else Constant(1)
              coefsAndNames = coefsAndNames :+ (coef, name)
            }
            case _ => throw new Exception(s"Failed to match '$considerCoef' as coefficient and name of var in equation")
          }
        
        equLeft = equLeft.substring(cnt)
        cnt = 0
      }
    }

    // Parse value
    val value = parse(valueS)
    
    new EquDef(coefsAndNames, value)
  }
}
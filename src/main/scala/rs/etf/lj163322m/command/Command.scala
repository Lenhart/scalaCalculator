package main.scala.rs.etf.lj163322m.command

import main.scala.rs.etf.lj163322m.expression._

object Command {
  def execute(expression: Expression): Unit = {
    expression match {
      case NaN =>
      case UnEqu => {
        expression()
      }
      case _: NamedVar => {
        expression()
      }
      case _: FunDef => {
        expression()
      }
      case _: EquDef => {
        expression()
      }
      case _: Solve => {
        expression()
      }
      case _ => {
        val value = expression()
        println(value)
      }
    }
  }
}

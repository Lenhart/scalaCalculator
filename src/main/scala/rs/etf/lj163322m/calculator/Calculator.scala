package main.scala.rs.etf.lj163322m.calculator

import main.scala.rs.etf.lj163322m.expression._
import scala.collection.mutable.HashMap
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.HashSet
import scala.collection.mutable.Map
import scala.concurrent._
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.annotation.tailrec

object Calculator {
  private val IntegrationWorkers = 10
  
  ///////////////
  // functions //
  ///////////////
  val funcByName = new HashMap[String, Double => Double]();
  
  private def oneOverX(x: Double) = 1 / x
  
  funcByName.put("exp", Math.exp);
  funcByName.put("log", Math.log);
  funcByName.put("signum", Math.signum)
  // parentheses
  funcByName.put("", (x) => x);
  // trigonometry
  funcByName.put("sin", Math.sin);
  funcByName.put("cos", Math.cos);
  funcByName.put("tan", Math.tan);
  funcByName.put("ctg", oneOverX _ compose Math.tan _) // (x) => 1 / Math.tan(x))
  // inverse trigonometry
  funcByName.put("asin", Math.asin);
  funcByName.put("acos", Math.acos);
  funcByName.put("atan", Math.atan);
  funcByName.put("actg", Math.atan _ compose oneOverX _) // (x) => Math.atan(1 / x)
  // additional stuff
  funcByName.put("unit", (x) => 1.0);
  funcByName.put("sqrt", Math.sqrt);
  
  ////////////////////////////
  // variables and contants //
  ////////////////////////////
  val valueByName = new HashMap[String, (Boolean, Double => Double)]
  
  ///////////////
  // equations //
  ///////////////
  val equations = new ArrayBuffer[EquDef]()
  
  def solve(equationIndices: Array[Int]): Array[(String, Double)] = {
    var equations = Array.empty[EquDef]
    for (index <- equationIndices) {
      val equation = Calculator.equations(index - 1)
      equations = equations :+ equation
    }
    solveEquDefs(equations)
  }
  
  private def solveEquDefs(equations: Array[EquDef]): Array[(String, Double)] = {
    val varNames = collectVarNames(equations)
    val mappedEquations = mapEquations(equations, varNames)
    
    if (varNames.size != mappedEquations.size) { // note + 1 because we're adding free element to the map too
      throw new Exception("I can solve only systems with number of equations being equal to number of variables")
    }

    val solved = solveMappedEqus(mappedEquations)
    var result = Array.empty[(String, Double)]
    for (i <- 0 until varNames.size) {
      result = result :+ (varNames(i), solved(i))
    }
    result
  }
  
  /**
   * Collects all variable names. For example, if we have system like this:
   * x + y = 0
   * y + z = 0
   * Resulting set should be: [x, y, z]
   */
  private def collectVarNames(equations: Array[EquDef]): Array[String] = {
    val varNames = HashSet.empty[String]
    for (equation <- equations) {
      for (coef <- equation.parts) {
        varNames.add(coef._2)
      }
    }
    
    val arrayNames = varNames.toArray[String]
    
    arrayNames.sortWith(_ < _)
  }
  
  private def mapEquations(equations: Array[EquDef], varNames: Array[String]): Map[Int, (Array[Double], Double)] = {
    val equMap = Map.empty[Int, (Array[Double], Double)]
    var index = 1
    for (equation <- equations) {
      var coefArray = Array.emptyDoubleArray
      
      for (name <- varNames) {
        val coefOpt = equation.parts.find((p) => p._2 == name)
        coefOpt match {
          case Some(coef) => coefArray = coefArray :+ coef._1()
          case None => coefArray = coefArray :+ 0.0
        }
      }
      
      val mappedEquation = (coefArray, equation.value())
      equMap.put(index, mappedEquation)
      index += 1
    }
    equMap
  }
  
  private def solveMappedEqus(system: Map[Int, (Array[Double], Double)]): Array[Double] = {
    // Solves a simple (1 unknown equation)
    def solveSingle(equation: (Array[Double], Double)): Double = {
      val coef = equation._1(0)
      val free = equation._2
      if (coef == 0) {
        if (free == 0)
          throw new Exception("System is too loose, there's infinite number of solutions")
        else
          throw new Exception("System is too strict, there's no solution")
      }
      free / coef
    }
    
    // Calculates value of all coefficients and builds a simple (1 unknown) equation.
    def swapValues(equation: (Array[Double], Double), solutions: Array[Double]): (Array[Double], Double) = {
      val coeff = Array(equation._1(0))
      var free = equation._2
      for (i <- 0 until solutions.size) {
        val i1 = i + 1
        free -= equation._1(i + 1) * solutions(i)
      }
      (coeff, free)
    }
    
    // Simplifies a system of equations using Gauusing elimination method
    // Left-most unknown gets eliminated
    def simplify(complexSystem: Map[Int, (Array[Double], Double)], oneEquation: (Array[Double], Double)): Map[Int, (Array[Double], Double)] = {
      val simplifiedSystem = Map.empty[Int, (Array[Double], Double)]
      for ((key, equation) <- complexSystem) {
        if (key != 1) {
          if (oneEquation._1(0) == 0 || equation._1(0) == 0) {
            throw new Exception("System is too loose, there's infinite number of solutions")
          }
          val eliminationMultiplier = - (equation._1(0) / oneEquation._1(0))
          var coefs = Array.emptyDoubleArray
          for (i <- 1 until oneEquation._1.size) {
            coefs = coefs :+ oneEquation._1(i) * eliminationMultiplier + equation._1(i)
          }
          val free = oneEquation._2 * eliminationMultiplier + equation._2
          val newEquation = (coefs, free)
          simplifiedSystem.put(key - 1, newEquation)
        }
      }
      simplifiedSystem
    }
    
    if (system.size == 0) {
      Array.empty[Double]
    } else {
      val eqOpt = system.get(1)
      eqOpt match {
        case Some(oneEquation) => {
          if (system.size == 1) {
            val solution = solveSingle(oneEquation)
            Array(solution)
          } else {
            // simplify system, eliminate left-most unknown
            val simplifiedSystem = simplify(system, oneEquation)
            // do recursive call using simplified system
            val solution = solveMappedEqus(simplifiedSystem)
            
            // swap unknowns with solutions and solve a simple equation, thus solve left-most unknown
            val simpleEquation = swapValues(oneEquation, solution)
            val simpleSolution = solveSingle(simpleEquation)
            
            // prepend solution to previously calculated one as a result
            simpleSolution +: solution
          }
        }
        case None => throw new Exception("Failed to get equation from map!")
      }
    }
  }
  
  def integrate(funcName: String, from: Double, to: Double): Double = {
    import scala.concurrent.ExecutionContext.Implicits.global
    val function = Calculator.funcByName.get(funcName) match {
      case Some(func) => func
      case None => throw new Exception(s"Unknown function named $funcName")
    }
    integrate(function, from, to)
  }
  
  def integrate(f: Double => Double, from: Double, to: Double): Double = {
    def integratePart(f: Double => Double, from: Double, to: Double): Double = {
      @tailrec
      def integrateImpl(sum: Double, f: Double => Double, from: Double, to: Double, step: Double): Double = {
        if (from >= to)
          sum
        else
          integrateImpl(sum + f(from) * step, f, from + step, to, step)
      }
      
      val defStep = 0.1
      val stepStep = Math.abs(to - from) / 100
      val stepSize = Math.min(defStep, stepStep)
      
      if (from <= to)
        integrateImpl(0, f, from, to, stepSize)
      else
        integrateImpl(0, f, to, from, stepSize)
    }

    var futures = Array.empty[Future[Double]]
    var step = (to - from) / IntegrationWorkers
    var currentFrom = from
    for (i <- 1 to IntegrationWorkers) {
      val myFrom = currentFrom
      val currentTo = currentFrom + step
      futures = futures :+ Future {
        integratePart(f, myFrom, currentTo)
      }
      currentFrom = currentTo
    }
    
    var total = 0.0
    for (future <- futures) {
      total = total + Await.result(future, 1 second)
    }
    total
  }
}
package main.scala.rs.etf.lj163322m

import scala.io.StdIn
import main.scala.rs.etf.lj163322m.parser._
import main.scala.rs.etf.lj163322m.calculator._
import main.scala.rs.etf.lj163322m.expression._
import main.scala.rs.etf.lj163322m.command._

object Main {
  def main(args: Array[String]) : Unit = {
    println("What can I calculate for you?")
    println("For usage, type 'help'")
    while (true) {
      try {
        val line = StdIn.readLine()
        line match {
          case "exit" | "\\q" => return
          case "help" | "?" => printHelp
          case "listFunc" => printFuncs
          case "listVars" => printVars(false)
          case "listVals" => printVars(true)
          case "listEqus" => printEqus
          case _ => {
            val expression = Parser.parse(line)
            Command.execute(expression)
          }
        }
      } catch {
        case e: Exception => {
          println(e.getMessage)
        }
      }
    }
  }
  
  private def printHelp() {
    println("""

You can type following commands:

exit      - to stop the program
\q        -      -||-
help      - shows this info
?         -      -||-
listFunc  - show list of available functions
listVars  - show list of available variables
listVals  - show list of available constants
listEqus  - show list of available equations

You can use constant integers and doubles
You can use following operators: +, -, *, /, ^ (note, power will still be associative left to right)

You can define local variable like this: $var1 = <expression> (note, name must start with $)
Expression is anything that can have calculated value, such as: 42, 1 + 1, sin(20),...
You can define constant like this: CONST1 = <expression>
You can then use them like this: 1 + $var1 + CONST1
You can define variables (unknowns like this): def x
You can define your own functions like this: myFunc$ = 2 * x
You can define equation like this: eq 3 * x = 0
You can solve equation #1 like this: solve 1
You can integrate function over range like this: integ sin log(2) 10

""")
  }
  
  private def printFuncs() {
    val funcs = Calculator.funcByName.keySet
    println("Following functions are defined:")
    for (fun <- funcs if "" != fun) println(fun)
  }
  
  private def printVars(fixed: Boolean) {
    val valOrVal = if (fixed) "constants" else "variables"
    println(s"Following $valOrVal are defined")
    for ((key, value) <- Calculator.valueByName if value._1 == fixed) {
      println(s"$key\t\t" + value._2(0))
    }
  }
  
  private def printEqus() {
    for (equ <- Calculator.equations) {
      println(equ.toString())
    }
  }
}